# sandboxql/sandboxql-client

The client-side Vue.js app for sandboxql.

## Setup
Create a .env.local file (example included) with your environment variables.

### Compiles and hot-reloads for development

```bash
docker-compose up
```

### Re-build the image
```bash
docker-compose up --build
```

### Compiles and minifies for production
```
docker exec -it sandboxql-client_serverless_1 yarn run build
```

### Run your tests
```
docker exec -it sandboxql-client_serverless_1 yarn run test
```

### Lints and fixes files
```
docker exec -it sandboxql-client_serverless_1 yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

#### Notes
You can access the shell for our image with the following command:

```bash
docker exec -it sandboxql-client_serverless_1 /bin/ash
```

`/bin/ash` is Ash ([Almquist Shell](https://www.in-ulm.de/~mascheck/various/ash/#busybox)) provided by BusyBox

### Todo
- Change cog icon to list button
- Feature: scopes() should be it's own mixin or whatever
- Feature: b-nav-item-dropdown id="edit-group" should be it's own component
- Feature: GroupTable.vue subgroups() method should return length
- Feature: Group edit parent
- Feature: Delete group
- Feature: Group member permissions
    -- View
    -- Add
    -- Edit
- Feature: User settings
- Feature: User profile with image upload
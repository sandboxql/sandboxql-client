import './assets/style.scss';
import './registerServiceWorker';
import { apolloProvider } from './vue-apollo';
import App from './App.vue';
import BootstrapVue from 'bootstrap-vue';
import router from './router';
import Vue from 'vue';
import Vuelidate from 'vuelidate';

Vue.use(BootstrapVue);
Vue.use(Vuelidate);

Vue.config.productionTip = false;

new Vue({
  router,
  apolloProvider,
  render: h => h(App)
}).$mount('#app');

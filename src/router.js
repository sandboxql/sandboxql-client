import Vue from 'vue';
import Router from 'vue-router';
import UpdateAlert from './graphql/UpdateAlert.gql';
import UpdateErrors from './graphql/UpdateErrors.gql';
import Account from './views/Account.vue';
import UserActivate from './views/Account/User/Activate.vue';
import UserRegister from './views/Account/User/Register.vue';
import UserSignin from './views/Account/User/Signin.vue';
import Dashboard from './views/Dashboard.vue';
import EditGroup from './views/Group/EditGroup.vue';
import GroupDetail from './views/Group/GroupDetail.vue';
import GroupList from './views/Group/GroupList.vue';
import NewGroup from './views/Group/NewGroup.vue';
import MemberDetail from './views/Member/MemberDetail.vue';
import NotFound from './views/NotFound.vue';
import { apolloProvider } from './vue-apollo';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Account,
      meta: { access: 'guest' },
      children: [
        {
          path: '/activate/:id',
          name: 'activate',
          component: UserActivate,
          props: true
        },
        {
          path: '/register',
          name: 'register',
          component: UserRegister
        },
        {
          path: '/signin',
          redirect: { name: 'signin' }
        },
        {
          path: '',
          name: 'signin',
          component: UserSignin
        }
      ]
    },
    {
      path: '/dashboard',
      component: Dashboard,
      children: [
        {
          path: 'group/:id',
          name: 'group-detail',
          component: GroupDetail,
          props: true
        },
        {
          path: 'group/:id/edit',
          name: 'group-edit',
          component: EditGroup,
          props: true,
          meta: { access: 'user' }
        },
        {
          path: 'group/:id/members',
          name: 'group-members',
          component: MemberDetail,
          props: true,
          meta: { access: 'user' }
        },
        {
          path: 'group/new',
          name: 'group-new',
          component: NewGroup,
          props: (route) => ({ parentId: route.query.parentId }),
          meta: { access: 'user' }
        },
        {
          path: '',
          name: 'group-list',
          component: GroupList,
          meta: { access: 'user' }
        }
      ]
    },
    {
      path: '*',
      name: 'notFound',
      component: NotFound
    }
  ]
});

router.beforeEach((to, from, next) => {
  let apolloClient = apolloProvider.defaultClient;
  apolloClient.mutate({
    mutation: UpdateAlert,
    variables: {
      message: '',
      variant: 'success',
      show: false
    }
  });

  apolloClient.mutate({
    mutation: UpdateErrors,
    variables: {
      input: []
    }
  });

  let token = JSON.parse(localStorage.getItem('auth-token')); // update this to use the apollo store
  if (to.matched.some(record => record.meta.access === 'user')) {
    if (token === null) {
      next({ name: 'signin' });
    }
  } else if (to.matched.some(record => record.meta.access === 'guest')) {
    if (token !== null) {
      next({ name: 'group-list' });
    }
  }
  next();
})

export default router;

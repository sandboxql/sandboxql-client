FROM node:alpine

RUN apk update

RUN yarn global add http-server && \
    yarn global add @vue/cli && \
    yarn install